import React from 'react';
import { Link } from 'react-router'
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import Badge from 'material-ui/Badge';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import NotificationIcon from 'material-ui/svg-icons/social/notifications-none';
import ChatIcon from 'material-ui/svg-icons/communication/chat';
import LoginIcon from 'material-ui/svg-icons/action/lock-open';
import LogoutIcon from 'material-ui/svg-icons/action/lock-outline';
import RegisterIcon from 'material-ui/svg-icons/content/create';
import HomeIcon from 'material-ui/svg-icons/action/home';
import FitnessCenter from 'material-ui/svg-icons/places/fitness-center';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import RaisedButton from 'material-ui/RaisedButton';
import {red900, red50} from 'material-ui/styles/colors';
import {ToolbarGroup, ToolbarSeparator} from 'material-ui/Toolbar';

const navStyles = {
    backgroundColor: '#F44336',
    position: 'fixed'
};

const iconMenuStyle = {
    padding: 12
};

const inviteBadgeStyle = {
    position: 'absolute'
};


export default class Header extends React.Component {
    state = {
        valueSingle: '3',
        valueMultiple: ['3', '5']
    };

    handleChangeSingle = (event, value) => {
        this.setState({
            valueSingle: value
        });
    };

    handleChangeMultiple = (event, value) => {
        this.setState({
            valueMultiple: value
        });
    };

    handleOpenMenu = () => {
        this.setState({
            openMenu: true
        });
    };

    handleOnRequestChange = (value) => {
        this.setState({
            openMenu: value
        });
    };


    render() {
        return (
            <AppBar
                title="Nasza-Klata"
                style={navStyles}
                iconElementLeft={<IconButton><FitnessCenter /></IconButton>}
                iconElementRight={
                        <div>
                            {/*
                            <Avatar
                              src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9"
                              size={35}
                              style={style}
                            />
                            */}

                            <IconButton containerElement={<Link to="/"/>}>
                                <HomeIcon color={red50}
                                />
                            </IconButton>
                            <IconMenu
                              iconButtonElement={
                                <Badge
                                  style={iconMenuStyle}
                                  badgeContent={4}
                                  badgeStyle={inviteBadgeStyle}
                                  primary={true}
                                >
                                    <IconButton>
                                        <PeopleIcon color={red50} />
                                    </IconButton>
                                 </Badge>
                              }
                              onChange={this.handleChangeSingle}
                              value={this.state.valueSingle}
                            >
                              <MenuItem value="1" primaryText="Refresh" />
                              <MenuItem value="2" primaryText="Send feedback" />
                              <MenuItem value="3" primaryText="Settings" />
                            </IconMenu>
                            <IconMenu
                              iconButtonElement={
                                <Badge
                                  style={iconMenuStyle}
                                  badgeContent={4}
                                  primary={true}
                                >
                                    <IconButton>
                                        <ChatIcon color={red50} />
                                    </IconButton>
                                </Badge>
                              }
                              onChange={this.handleChangeMultiple}
                              value={this.state.valueMultiple}
                              multiple={true}
                            >
                              <MenuItem value="1" primaryText="Blu-ray" />
                              <MenuItem value="2" primaryText="Cassette" />
                              <MenuItem value="3" primaryText="CD" />
                              <MenuItem value="4" primaryText="DVD Audio" />
                              <MenuItem value="5" primaryText="Hybrid SACD" />
                              <MenuItem value="6" primaryText="Vinyl" />
                            </IconMenu>
                            <IconMenu
                              iconButtonElement={
                                <Badge
                                  style={iconMenuStyle}
                                  badgeContent={4}
                                  primary={true}
                                >
                                    <IconButton>
                                        <NotificationIcon color={red50} />
                                    </IconButton>
                                </Badge>
                              }
                              open={this.state.openMenu}
                              onRequestChange={this.handleOnRequestChange}
                            >
                              <MenuItem value="1" primaryText="Windows App" />
                            </IconMenu>
                        </div>
                }
            >
                <ToolbarGroup>
                    <FontIcon className="muidocs-icon-custom-sort"/>
                    <ToolbarSeparator />
                    <RaisedButton
                        label="Login"
                        icon={<LoginIcon color={red50} />}
                        backgroundColor='#B71C1C'
                        disabledBackgroundColor='#B71C1C'
                        labelColor={red50}
                        containerElement={<Link to="/Login"/>}
                    />
                    <RaisedButton
                        label="Logout"
                        icon={<LogoutIcon color={red50} />}
                        backgroundColor={red900}
                        labelColor={red50}
                    />
                    <RaisedButton
                        label="Register"
                        icon={<RegisterIcon color={red50} />}
                        backgroundColor={red900}
                        labelColor={red50}
                        containerElement={<Link to="/register"/>}
                    />
                    <IconMenu
                        iconButtonElement={
                          <IconButton touch={true}>
                            <NavigationExpandMoreIcon color={red50} />
                          </IconButton>
                        }
                    >
                        <MenuItem primaryText="Download"/>
                        <MenuItem primaryText="More Info"/>
                    </IconMenu>
                </ToolbarGroup>
            </AppBar>

        );
    }
}