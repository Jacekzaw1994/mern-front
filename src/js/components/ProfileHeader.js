import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import PhotosIcon from 'material-ui/svg-icons/image/photo-library';
import FriendsIcon from 'material-ui/svg-icons/social/people-outline';
import PostsIcon from 'material-ui/svg-icons/action/view-list';


const postsIcon = <PostsIcon />
const friendsIcon = <FriendsIcon />
const photosIcon =  <PhotosIcon />

export default class ProfileHeader extends React.Component {

    state = {
        selectedIndex: 0,
    };

    select = (index) => this.setState({selectedIndex: index});

    render() {
        return (
            <div>
                <Card>
                    <CardMedia
                        overlay={<CardTitle title="Jacek Zawodny" />}
                    >
                        <img src="https://i.ytimg.com/vi/DGcHZBsPt6g/maxresdefault.jpg"/>
                    </CardMedia>
                </Card>
                <Paper zDepth={1}>
                    <BottomNavigation selectedIndex={this.state.selectedIndex}>
                        <BottomNavigationItem
                            label="Posts"
                            icon={postsIcon}
                            onTouchTap={() => this.select(0)}
                        />
                        <BottomNavigationItem
                            label="Friends"
                            icon={friendsIcon}
                            onTouchTap={() => this.select(1)}
                        />
                        <BottomNavigationItem
                            label="Photos"
                            icon={photosIcon}
                            onTouchTap={() => this.select(2)}
                        />
                    </BottomNavigation>
                </Paper>
            </div>
        );
    }
}