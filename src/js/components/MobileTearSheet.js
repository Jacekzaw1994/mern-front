import React from 'react';

export default class MobileTearSheet extends React.Component {

    render() {

        let styles = {
            root: {
                marginBottom: 24,
                marginRight: 24,
                position: 'fixed'
            },

            container: {
                border: 'solid 1px #d9d9d9',
                height: this.props.height,
                overflow: 'hidden'
            },

            bottomTear: {
                display: 'block',
                position: 'relative',
                marginTop: -5,
                width: '100%'
            }
        };

        return (
            <div style={styles.root}>
                <div style={styles.container}>
                    {this.props.children}
                </div>
            </div>
        );
    }

};

MobileTearSheet.getDefaultProps = {height: 500};

MobileTearSheet.propTypes = {
    height: React.PropTypes.number
};