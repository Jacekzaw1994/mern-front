import React from "react";

const aroundStyle = {
    padding: 5,
    backgroundColor: '#B71C1C',
    marginTop: 4,
    marginBottom: 4
};

const textStyle = {
    color: '#FFF'
};

export default class MyMessage extends React.Component {
    render() {
        return (
          <div style={aroundStyle}>
              <p style={textStyle}>Default example message</p>
          </div>
        );
    }
}