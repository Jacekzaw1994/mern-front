import React from 'react';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

const stylePaperImage = {
    height: 100,
    width: 100,
    textAlign: 'center',
    display: 'inline-block'
};

const profilePhotoStyle = {
    width: 100
};

const profileNameStyle = {
    fontFamily: 'Lato, Sans-serif',
    fontSize: 30,
    fontWeight: 'bold',
    paddingLeft: 15,
    float: 'right',
    width: '60%'
};

const paperListStyle = {
    width: '100%'
};

const divStyle = {
    width: '100%'
};

const PaperExampleRounded = () => (
    <div>
        <div style={divStyle}>
            <Paper style={stylePaperImage} zDepth={5} rounded={false}>
                <img style={profilePhotoStyle}
                     src="https://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_400x400.png"
                     alt="profile photo"/>
            </Paper>
            <div style={profileNameStyle}>Jacek Zawodny</div>
        </div>
        <div style={divStyle}>
            <Paper style={paperListStyle}>
                <Menu>
                    <MenuItem primaryText="Home page" />
                    <MenuItem primaryText="Posts" />
                    <MenuItem primaryText="Photos" />
                    <MenuItem primaryText="Friends" />
                    <MenuItem primaryText="Details" />
                </Menu>
            </Paper>
        </div>
    </div>

);

export default PaperExampleRounded;