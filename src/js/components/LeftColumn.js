import React from 'react';
import MobileTearSheet from './MobileTearSheet';
import {List, ListItem} from 'material-ui/List';
import {red500} from 'material-ui/styles/colors'
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import Divider from 'material-ui/Divider';
import ActionInfo from 'material-ui/svg-icons/action/info';



const style = {
    height: 100,
    width: 100,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block'
};

const avatarStyle = {
    color: '#757575',
    fontSize: '24px',
    fontFamily: 'Roboto, sans-serif'
};

const deviderStyle = {
    color: '#757575',
    backgroundColor: '#757575'
};

const listItemStyle = {
    color: '#212121'
};

export default class LeftColumn extends React.Component {

    render() {
        return (
            <MobileTearSheet>
                <List>
                    <ListItem
                        style={avatarStyle}
                        disabled={true}
                        leftAvatar={
                            <Avatar src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9" />
                          }
                    >
                        Jacek Zawodny
                    </ListItem>
                    <ListItem primaryText="Inbox" style={listItemStyle} hoverColor={red500} leftIcon={<ContentInbox />}/>
                    <ListItem primaryText="Starred" style={listItemStyle} hoverColor={red500} leftIcon={<ActionGrade />}/>
                    <ListItem primaryText="Sent mail" style={listItemStyle} hoverColor={red500} leftIcon={<ContentSend />}/>
                    <ListItem primaryText="Drafts" style={listItemStyle} hoverColor={red500} leftIcon={<ContentDrafts />}/>
                    <ListItem primaryText="Inbox" style={listItemStyle} hoverColor={red500} leftIcon={<ContentInbox />}/>
                </List>
                <Divider style={deviderStyle}/>
                <List>
                    <ListItem primaryText="All mail" style={listItemStyle} hoverColor={red500} rightIcon={<ActionInfo />}/>
                    <ListItem primaryText="Trash" style={listItemStyle} hoverColor={red500} rightIcon={<ActionInfo />}/>
                    <ListItem primaryText="Spam" style={listItemStyle} hoverColor={red500} rightIcon={<ActionInfo />}/>
                    <ListItem primaryText="Follow up" style={listItemStyle} hoverColor={red500} rightIcon={<ActionInfo />}/>
                </List>
            </MobileTearSheet>
        );
    }
};