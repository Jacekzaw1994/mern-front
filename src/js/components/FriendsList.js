import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    },
    gridList: {
        width: '100%',
        overflowY: 'auto'
    }
};

const tilesData = [
    {
        img: 'https://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_400x400.png',
        title: 'Breakfast',
        author: '120'
    },
    {
        img: 'http://www.newsweek.pl/g/i.aspx/680/0/newsweek/635757801295499590.jpg',
        title: 'Tasty burger',
        author: '120'
    },
    {
        img: 'http://6.s.dziennik.pl/pliki/8135000/8135767-andrzej-duda-900-600.jpg',
        title: 'Camera',
        author: '120'
    },
    {
        img: 'http://x3.cdn03.imgwykop.pl/c3201142/comment_nM7iOLGEsdLJLDXwgeGP32fHk9GApKeI.gif',
        title: 'Morning',
        author: '120'
    },
    {
        img: 'http://gfx.radiozet.pl/var/radiozetv3/storage/images/wiadomosci/kraj/kiedy-andrzej-duda-zostanie-oficjalnie-prezydentem-kalendarz-00005799/610676-1-pol-PL/Kiedy-Andrzej-Duda-zostanie-oficjalnie-prezydentem-Kalendarz.jpg',
        title: 'Hats',
        author: '120'
    },
    {
        img: 'http://slib.pl/wp-content/uploads/2015/08/duda.jpg',
        title: 'Honey',
        author: '120'
    },
    {
        img: 'http://www.elblag.net/img/artykuly/17092_do-elblaga-zawita-andrzej-duda-kandydat-na_1.jpg?1424869015',
        title: 'Vegetables',
        author: '120'
    },
    {
        img: 'https://upload.wikimedia.org/wikipedia/commons/b/b0/Andrzej_Duda.jpg',
        title: 'Water plant',
        author: '120'
    }
];


const FriendsList = () => (
    <div style={styles.root}>
        <GridList
            cellHeight={180}
            style={styles.gridList}
        >
            {tilesData.map((tile) => (
                <GridTile
                    key={tile.img}
                    title={tile.title}
                    subtitle={<span><b>{tile.author}</b> friends</span>}
                    actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
                >
                    <img src={tile.img} />
                </GridTile>
            ))}
        </GridList>
    </div>
);

export default FriendsList;