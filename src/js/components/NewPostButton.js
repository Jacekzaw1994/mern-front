import React from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AddPostIcon from 'material-ui/svg-icons/editor/border-color';
import Paper from 'material-ui/Paper';

const textFliedStyle = {
    width: '100%'
};

const paperStyle = {
    height: 60,
    marginBottom: 20,
    width: '100%',
    padding: 10,
    textAlign: 'center',
    display: 'inline-block'
};

const popupTextFieldButtonStyle = {
    marginLeft: 20
};

export default class NewPostButton extends React.Component {
    state = {
        open: false
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleClose}
            />
        ];

        return (
            <div>
                <Paper style={paperStyle} zDepth={2}>
                    Click button to add new post
                    <RaisedButton
                        style={popupTextFieldButtonStyle}
                        target="_blank"
                        label="WRITE NEW POST"
                        secondary={true}
                        icon={<AddPostIcon />}
                        onTouchTap={this.handleOpen}
                    />
                </Paper>
                <Dialog
                    title="Add post"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    <TextField
                        style={textFliedStyle}
                        hintText="Write new post"
                        multiLine={true}
                        rows={4}
                        rowsMax={10}
                    />
                </Dialog>
            </div>
        );
    }
}