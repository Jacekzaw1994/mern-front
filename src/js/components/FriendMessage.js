import React from "react";

const aroundStyle = {
    padding: 5,
    backgroundColor: '#E0E0E0',
    marginTop: 4,
    marginBottom: 4
};

const textStyle = {
    color: '#212121'
};

export default class FriendMessage extends React.Component {
    render() {
        return (
            <div style={aroundStyle}>
                <p style={textStyle}>Default example message</p>
            </div>
        );
    }
}