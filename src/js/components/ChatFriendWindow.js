import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FriendMessage from './FriendMessage';
import MyMessage from './MyMessage';
import TextField from 'material-ui/TextField';

const headerStyle = {
    backgroundColor: '#F44336',
    paddingTop: 8,
    paddingBottom: 8
};

const cardActionStyle = {
    paddingTop: 0,
    paddingBottom: 0
};
const textFieldStyle = {
    width: '100%'
};

const cardTextStyle = {
    backgroundColor: "#FFF",
    minHeight: 200,
    maxHeight: 200,
    padding: '4px 8px',
    overflowY: 'scroll',
    overflowX: 'hidden'
};

export default class ChatFriendWindow extends React.Component {


    render() {
        return (
            <Card>
                <CardHeader
                    style={headerStyle}
                    title="Friend Name"
                />
                <CardText style={cardTextStyle}>
                    <FriendMessage />
                    <MyMessage />
                    <FriendMessage />
                    <MyMessage />
                    <FriendMessage />
                    <MyMessage />
                    <FriendMessage />
                    <MyMessage />
                    <FriendMessage />
                    <MyMessage />
                    <FriendMessage />
                    <MyMessage />
                </CardText>
                <CardActions style={cardActionStyle}>
                    <TextField
                        style={textFieldStyle}
                        hintText="Write a message..."
                        multiLine={true}
                        rows={1}
                        rowsMax={2}
                    />
                </CardActions>
            </Card>
        )
    }
}