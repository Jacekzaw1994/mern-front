import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

const cardStyle = {
    marginBottom: 20,
    width: '100%'
};

export default class PostItem extends React.Component {

    render() {
        return (
            <Card style={cardStyle}>
                <CardHeader
                    title="URL Avatar"
                    subtitle="Subtitle"
                    avatar="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9"
                />
                <CardMedia
                    overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle"/>}
                >
                    <img src="http://www.cycleworld.com/sites/cycleworld.com/files/styles/medium_1x_/public/wp-content/uploads/2015/12/2016-KTM-Duke-690-studio-2.jpg?itok=NchW1fRI"/>
                </CardMedia>
                <CardTitle title="Card title" subtitle="Card subtitle"/>
                <CardText>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                    Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                    Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                </CardText>
                <CardActions>
                    <FlatButton label="Action1"/>
                    <FlatButton label="Action2"/>
                </CardActions>
            </Card>
        );
    }
}