import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader';
import ChatIcon from 'material-ui/svg-icons/communication/chat';
import {List, ListItem} from 'material-ui/List';
import {limeA400} from 'material-ui/styles/colors';
import CommunicationChatBubble from 'material-ui/svg-icons/communication/chat-bubble';
import ChatFriendWindow from './ChatFriendWindow';

const drawerStyles = {
    left: 'none',
    right: '0px'
};

const chatHeaderStyle = {
    lineHeight: 2
};

export default class Chat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {open: false};
    }


    handleToggle = () => this.setState({open: !this.state.open});

    render() {
        return (
            <div>
                <RaisedButton
                    icon={<ChatIcon />}
                    label="Toggle Chat"
                    onTouchTap={this.handleToggle}
                />
                <Drawer open={this.state.open} styles={drawerStyles}>
                    <List>
                        <Subheader style={chatHeaderStyle}>Chat Friends</Subheader>
                        <ListItem
                            primaryText="Brendan Lim"
                            leftAvatar={<Avatar src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9" />}
                            rightIcon={<CommunicationChatBubble color={limeA400}/>}
                        />
                        <ListItem
                            primaryText="Eric Hoffman"
                            leftAvatar={<Avatar src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                        <ListItem
                            primaryText="Grace Ng"
                            leftAvatar={<Avatar src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9" />}
                            rightIcon={<CommunicationChatBubble color={limeA400} />}
                        />
                        <ListItem
                            primaryText="Kerem Suer"
                            leftAvatar={<Avatar src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                        <ListItem
                            primaryText="Raquel Parrado"
                            leftAvatar={<Avatar src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/1395333_811215075557637_6438275851237123928_n.jpg?oh=fb307c1c2f2dc83591d568cac1203778&oe=58B650E9" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                    </List>
                    <div>
                        <ChatFriendWindow />
                    </div>
                </Drawer>
            </div>
        );
    }
}