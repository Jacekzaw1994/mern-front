import React from 'react';
import ReactDOM from 'react-dom';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import logger from "redux-logger";
import thunk from "redux-thunk";
import axios from "axios";
import Router from './Router';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();


const initialState = {
    fetching:  false,
    fetched: false,
    users: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_USERS_START":
        {
            return {...state, fetching: true};
            break;
        }
        case "FETCH_USERS_ERROR":
        {
            return {...state, fetching: false, error: action.payload};
            break;
        }
        case "RECEIVE_USERS":
        {
            return {...state, fetching: false, fetched: true, users: action.payload};
            break;
        }
    }
    return state;
};

const middleware = applyMiddleware(thunk, logger());

const store = createStore(reducer, middleware);


store.dispatch((dispatch) => {
    dispatch({type: "FETCH_USER_START"});
    axios.get("http://localhost:8081/api/users")
        .then((response) => {
            dispatch({type: "RECEIVE_USERS", payload: response.data});
        })
        .catch((err) => {
            dispatch({type: "FETCH_USERS_ERROR", payload: err})
        });
    //do something async
});

const App = () => (
    <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
        <Router />
    </MuiThemeProvider>
);

ReactDOM.render(
    <App />,
    document.getElementById('app')
);