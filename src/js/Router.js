import React from 'react';
import Layout from './Layout';
import Home from './pages/Home';
import ErrorPage from './pages/ErrorPage';
import MaterialUiForm from './pages/MaterialUiForm';
import Profile from './pages/Profile';
import Login from './pages/Login';
import { Router, Route, hashHistory, IndexRoute} from 'react-router';

export default () => (
    <Router history={hashHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={Home}/>
            <Route path="/profile" component={Profile} />
        </Route>
        <Route path="/login" components={Login} />
        <Route path="/register" components={MaterialUiForm} />
        <Route path="/*" components={ErrorPage} />
    </Router>
);