import React from 'react';
import { Link } from 'react-router'
import IconButton from 'material-ui/IconButton';
import ActionHome from 'material-ui/svg-icons/action/home';
import {Grid, Row, Col} from 'react-flexbox-grid';
import { red700, red900} from 'material-ui/styles/colors';

const errorPageStyle = {
    paddingTop: 100,
    textAlign: 'center'
};

const errorHeadingStyle = {
    fontFamily: 'Lato, Sans-serif',
    textTransform: 'uppercase',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#757575'
};

const error404Style = {
    fontSize: 300,
    fontFamily: 'Lato, Sans-serif',
    fontWeight: 'bold',
    color: '#616161',
    textShadow: '4px 2px 6px rgba(150, 150, 150, 0.92)'
};

const pStyle = {
    fontFamily: 'Lato, Sans-serif',
    fontWeight: 'bold',
    fontSize: 24,
    color: '#757575'
};

const styles = {
    largeIcon: {
        width: 60,
        height: 60
    },
    large: {
        width: 120,
        height: 120,
        padding: 30,
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
    }
};


export default class ErrorPage extends React.Component {

    render() {
        return (
            <Grid fluid={true} style={errorPageStyle}>
                <Row>
                    <Col lg={6} lgOffset={3}>
                        <span style={errorHeadingStyle}>Something went wrong!</span>
                        <h1 style={error404Style}>404</h1>
                        <p style={pStyle}>Page Not Found.</p>
                        <p style={pStyle}>Back to home page.</p>
                        <IconButton
                            iconStyle={styles.largeIcon}
                            style={styles.large}
                            containerElement={<Link to="/"/>}
                        >
                            <ActionHome color={red700} hoverColor={red900} />
                        </IconButton>
                    </Col>
                </Row>
            </Grid>
        );
    }
}