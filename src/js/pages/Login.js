import React from 'react';
import Formsy from 'formsy-react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import PasswordField from 'material-ui-password-field';
import {Grid, Row, Col} from 'react-flexbox-grid';
import {FormsyDate, FormsyRadio, FormsyRadioGroup, FormsyText} from 'formsy-material-ui/lib';

const emailStyle = {
    width: '100%'
};

const Main = React.createClass({

    getInitialState() {
        return {
            canSubmit: false
        };
    },

    errorMessages: {
        wordsError: "Please only use letters",
        numericError: "Please provide a number",
        urlError: "Please provide a valid URL"
    },

    styles: {
        paperStyle: {
            width: '100%',
            paddingTop: 100,
            marginTop: 50,
            padding: 20
        },
        switchStyle: {
            marginBottom: 16
        },
        submitStyle: {
            marginTop: 32
        }
    },

    enableButton() {
        this.setState({
            canSubmit: true
        });
    },

    disableButton() {
        this.setState({
            canSubmit: false
        });
    },

    submitForm(data) {
        alert(JSON.stringify(data, null, 4));
    },

    notifyFormError(data) {
        console.error('Form error:', data);
    },

    render() {
        let {paperStyle, switchStyle, submitStyle} = this.styles;
        let {wordsError} = this.errorMessages;

        return (
            <Grid>
                <Row>
                    <Col lg={4} lgOffset={4} >
                        <Paper style={paperStyle}>
                            <Formsy.Form
                                onValid={this.enableButton}
                                onInvalid={this.disableButton}
                                onValidSubmit={this.submitForm}
                                onInvalidSubmit={this.notifyFormError}
                            >
                                <FormsyText
                                    style={emailStyle}
                                    name="email"
                                    validations="isEmail"
                                    validationError="This is not an email"
                                    required
                                    hintText="Your email"
                                    floatingLabelText="Email"
                                />
                                <PasswordField
                                    hintText="At least 8 characters"
                                    floatingLabelText="Enter your password"
                                    errorText="Your password is too short"
                                />
                                <RaisedButton
                                    style={submitStyle}
                                    type="submit"
                                    label="LOGIN"
                                    disabled={!this.state.canSubmit}
                                />
                            </Formsy.Form>
                        </Paper>
                    </Col>
                </Row>
            </Grid>
        );
    }
});

export default Main;