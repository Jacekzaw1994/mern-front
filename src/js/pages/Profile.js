import React from 'react';
import ProfileHeader from './../components/ProfileHeader';
import Chat from './../components/Chat';
import PostItem from './../components/PostItem';
import FriendsList from './../components/FriendsList';
import Gallery from './../components/Gallery';
import ProfileDetails from './../components/ProfileDetails';

import {Grid, Row, Col} from 'react-flexbox-grid';

const profileStyle = {
    paddingTop: 100
};

const profileContentStyle = {
    marginTop: 30
};

export default class Profile extends React.Component {

    render() {
        return (
            <Grid fluid={true} style={profileStyle}>
                <Row>
                    <Col lg={3}>
                        <ProfileDetails />
                    </Col>
                    <Col lg={6}>
                        <Row>
                            <Col lg={12}>
                                <ProfileHeader />
                            </Col>
                            <Col lg={12} style={profileContentStyle}>
                                <PostItem />
                                <PostItem />
                                <PostItem />
                                <PostItem />
                            </Col>
                            <Col lg={12}>
                                <FriendsList />
                            </Col>
                            <Col lg={12}>
                                <Gallery />
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={3}>
                        <Chat />
                    </Col>
                </Row>
            </Grid>
        );
    }
}