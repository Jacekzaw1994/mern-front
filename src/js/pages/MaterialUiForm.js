import React from 'react';
import Formsy from 'formsy-react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import PasswordField from 'material-ui-password-field';
import {Grid, Row, Col} from 'react-flexbox-grid';
import MenuItem from 'material-ui/MenuItem';
import {
    FormsyCheckbox, FormsyDate, FormsyRadio, FormsyRadioGroup,
    FormsySelect, FormsyText, FormsyTime, FormsyToggle, FormsyAutoComplete
} from 'formsy-material-ui/lib';

const Main = React.createClass({

    getInitialState() {
        return {
            canSubmit: false
        };
    },

    errorMessages: {
        wordsError: "Please only use letters",
        numericError: "Please provide a number",
        urlError: "Please provide a valid URL"
    },

    styles: {
        paperStyle: {
            width: '100%',
            paddingTop: 100,
            marginTop: 50,
            padding: 20
        },
        switchStyle: {
            marginBottom: 16
        },
        submitStyle: {
            marginTop: 32
        }
    },

    enableButton() {
        this.setState({
            canSubmit: true
        });
    },

    disableButton() {
        this.setState({
            canSubmit: false
        });
    },

    submitForm(data) {
        alert(JSON.stringify(data, null, 4));
    },

    notifyFormError(data) {
        console.error('Form error:', data);
    },

    render() {
        let {paperStyle, switchStyle, submitStyle} = this.styles;
        let {wordsError, numericError, urlError} = this.errorMessages;

        return (
            <Grid>
                <Row>
                    <Col lg={6} lgOffset={3} >
                        <Paper style={paperStyle}>
                            <Formsy.Form
                                onValid={this.enableButton}
                                onInvalid={this.disableButton}
                                onValidSubmit={this.submitForm}
                                onInvalidSubmit={this.notifyFormError}
                            >
                                <FormsyText
                                    name="firstname"
                                    validations="isWords"
                                    validationError={wordsError}
                                    required
                                    hintText="What is your name?"
                                    floatingLabelText="First Name"
                                />
                                <FormsyText
                                    name="lastname"
                                    validations="isWords"
                                    validationError={wordsError}
                                    required
                                    hintText="What is your last name?"
                                    floatingLabelText="Last Name"
                                />
                                <FormsyText
                                    name="email"
                                    validations="isEmail"
                                    validationError="This is not an email"
                                    required
                                    hintText="Your email"
                                    floatingLabelText="Email"
                                />
                                <FormsyText
                                    name="reemail"
                                    validations="isEmail"
                                    validationError="This is not the same email"
                                    required
                                    hintText="Re-enter Email"
                                    floatingLabelText="Re-enter Email"
                                />
                                <PasswordField
                                    hintText="At least 8 characters"
                                    floatingLabelText="Enter your password"
                                    errorText="Your password is too short"
                                />
                                <FormsyDate
                                    name="date"
                                    required
                                    floatingLabelText="Date of birth"
                                />
                                <FormsyRadioGroup name="shipSpeed" defaultSelected="not_light">
                                    <FormsyRadio
                                        value="light"
                                        label="Male"
                                        style={switchStyle}
                                    />
                                    <FormsyRadio
                                        value="not_light"
                                        label="Female"
                                        style={switchStyle}
                                    />
                                </FormsyRadioGroup>
                                <RaisedButton
                                    style={submitStyle}
                                    type="submit"
                                    label="REGISTER"
                                    disabled={!this.state.canSubmit}
                                />
                            </Formsy.Form>
                        </Paper>
                    </Col>
                </Row>
            </Grid>
        );
    }
});

export default Main;