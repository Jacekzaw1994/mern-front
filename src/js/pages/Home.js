import React from 'react';
import Chat from './../components/Chat';
import LeftColumn from './../components/LeftColumn';
import NewPostButton from './../components/NewPostButton';
import PostItem from './../components/PostItem';
import {Grid, Row, Col} from 'react-flexbox-grid';

const homeStyle = {
  paddingTop: 100
};

export default class Home extends React.Component {
    render() {
        return (
            <Grid fluid={true} style={homeStyle}>
                <Row>
                    <Col lg={3}>
                        <LeftColumn />
                    </Col>
                    <Col lg={6}>
                        <NewPostButton />
                        <PostItem />
                        <PostItem />
                        <PostItem />
                        <PostItem />
                    </Col>
                    <Col md={3}>
                        <Chat />
                    </Col>
                </Row>
            </Grid>
        );
    }
}